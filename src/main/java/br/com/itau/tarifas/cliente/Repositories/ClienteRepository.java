package br.com.itau.tarifas.cliente.Repositories;

import br.com.itau.tarifas.cliente.Model.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Optional<Cliente> findByCpfCnpj(String cpfCnpj);

}
