package br.com.itau.tarifas.cliente.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "O Cliente não foi encontrado")
public class ClienteNotFoundException extends RuntimeException {
}
