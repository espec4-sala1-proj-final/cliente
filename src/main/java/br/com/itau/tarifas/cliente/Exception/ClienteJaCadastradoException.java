package br.com.itau.tarifas.cliente.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "O Cliente já foi cadastrado!!!")
public class ClienteJaCadastradoException extends RuntimeException {
}
