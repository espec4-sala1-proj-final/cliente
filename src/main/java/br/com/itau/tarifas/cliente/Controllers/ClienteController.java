package br.com.itau.tarifas.cliente.Controllers;

import br.com.itau.tarifas.cliente.Model.Cliente;
import br.com.itau.tarifas.cliente.Services.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/clientes")
@Api(value = "Microsservico de clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ApiOperation(value = "Incluir um novo cliente")
    public Cliente criar(@RequestBody Cliente cliente) {
        System.out.println("API Cliente - Criar um novo cliente");
        return clienteService.salvarCliente(cliente);
    }


    @GetMapping
    @ApiOperation(value = "Busca todos os clientes cadastrados")
    public Iterable<Cliente> lerTodosOsClientes(){
        System.out.println("API Cliente - Busca por todos os clientes");
        return clienteService.lerTodosOsClientes();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Busca um cliente por um ID")
    public Cliente buscarPorId(@PathVariable(name = "id") int id) {
        System.out.println("API Cliente - Busca por ID");
        try {
            return clienteService.buscarClientePorId(id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/documento/{cpfCnpj}")
    @ApiOperation(value = "Busca por CPF")
    public Cliente buscaPorCpfCnpj(@PathVariable String cpfCnpj) {
        System.out.println("API Cliente - Busca por CPF/CNPJ");
        try {
            return clienteService.pesquisarPorCpf(cpfCnpj);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }
}
