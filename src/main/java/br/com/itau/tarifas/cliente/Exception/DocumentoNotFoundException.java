package br.com.itau.tarifas.cliente.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "CPF/CNPJ não encontrado.")
public class DocumentoNotFoundException extends RuntimeException {
}
