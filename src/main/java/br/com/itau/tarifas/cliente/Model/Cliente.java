package br.com.itau.tarifas.cliente.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "CPF/CNPJ nao pode estar como vazio")
    @NotBlank(message =  "CPF/CNPJ não pode ser vazio")
    @Size(min = 11, message = "O CPF/CNPJ deve ter mais que 11 caracteres")
    private String cpfCnpj;

    @NotNull(message = "Nome nao pode estar como vazio")
    @NotBlank(message =  "Nome não pode ser vazio")
    @Size(min = 3, message = "O nome deve ter mais que 3 caracteres")
    private String nome;

    @NotNull(message = "o segmento não pode estar como vazio")
    @NotBlank(message =  "o segmento não pode estar em branco")
    private String segmento;

    public Cliente() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }
}
