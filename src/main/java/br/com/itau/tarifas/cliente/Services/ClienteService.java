package br.com.itau.tarifas.cliente.Services;

import br.com.itau.tarifas.cliente.Exception.ClienteJaCadastradoException;
import br.com.itau.tarifas.cliente.Exception.ClienteNotFoundException;
import br.com.itau.tarifas.cliente.Exception.DocumentoNotFoundException;
import br.com.itau.tarifas.cliente.Model.Cliente;
import br.com.itau.tarifas.cliente.Repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;


    //Salvar produto
    public Cliente salvarCliente(Cliente cliente){
        Optional<Cliente> clienteOptional = clienteRepository.findByCpfCnpj(cliente.getCpfCnpj());

        if(clienteOptional.isPresent()){
            throw new ClienteJaCadastradoException();
        }

        return clienteRepository.save(cliente);
    }

    //retorno de todos os registros
    public Iterable<Cliente> lerTodosOsClientes(){
        return clienteRepository.findAll();
    }

    //Retorno buscando por ID
    public Cliente buscarClientePorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }else {
            throw new ClienteNotFoundException();
        }
    }

    public Cliente pesquisarPorCpf(String cpfCnpf){
        Optional<Cliente> cliente = clienteRepository.findByCpfCnpj(cpfCnpf);
        if(!cliente.isPresent()){
            throw new DocumentoNotFoundException();
        }
        return cliente.get();
    }

}
