FROM openjdk:8-jre-alpine

WORKDIR /appagent
COPY /appagent/ ./

COPY target/Cliente-*.jar api-cliente.jar

CMD ["java", "-javaagent:/appagent/javaagent.jar" , "-jar", "api-cliente.jar"]